EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:hp_connector-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Conn_02x05_Top_Bottom J1
U 1 1 5AD10509
P 4400 3950
F 0 "J1" H 4450 4250 50  0000 C CNN
F 1 "Conn_02x05_Top_Bottom" H 4450 3650 50  0000 C CNN
F 2 "jacobs_footprints:connector_5x2_100mil" H 4400 3950 50  0001 C CNN
F 3 "" H 4400 3950 50  0001 C CNN
	1    4400 3950
	1    0    0    -1  
$EndComp
$Comp
L Conn_02x05_Top_Bottom J3
U 1 1 5AD10527
P 6500 3950
F 0 "J3" H 6550 4250 50  0000 C CNN
F 1 "Conn_02x05_Top_Bottom" H 6550 3650 50  0000 C CNN
F 2 "jacobs_footprints:connector_5x2_100mil" H 6500 3950 50  0001 C CNN
F 3 "" H 6500 3950 50  0001 C CNN
	1    6500 3950
	1    0    0    -1  
$EndComp
$Comp
L Conn_02x10_Top_Bottom J2
U 1 1 5AD10547
P 5400 3850
F 0 "J2" H 5450 4350 50  0000 C CNN
F 1 "Conn_02x10_Top_Bottom" H 5450 3250 50  0000 C CNN
F 2 "jacobs_footprints:connector_10x2_100mil" H 5400 3850 50  0001 C CNN
F 3 "" H 5400 3850 50  0001 C CNN
	1    5400 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 3750 4200 3450
Wire Wire Line
	4200 3450 5200 3450
Wire Wire Line
	5200 3550 5150 3550
Wire Wire Line
	5150 3550 5150 3400
Wire Wire Line
	5150 3400 4150 3400
Wire Wire Line
	4150 3400 4150 3850
Wire Wire Line
	4150 3850 4200 3850
Wire Wire Line
	4200 3950 4100 3950
Wire Wire Line
	4100 3950 4100 3350
Wire Wire Line
	4100 3350 5100 3350
Wire Wire Line
	5100 3350 5100 3650
Wire Wire Line
	5100 3650 5200 3650
Wire Wire Line
	4200 4050 4050 4050
Wire Wire Line
	4050 4050 4050 3300
Wire Wire Line
	4050 3300 5050 3300
Wire Wire Line
	5050 3300 5050 3750
Wire Wire Line
	5050 3750 5200 3750
Wire Wire Line
	4200 4150 4000 4150
Wire Wire Line
	4000 4150 4000 3250
Wire Wire Line
	4000 3250 5000 3250
Wire Wire Line
	5000 3250 5000 3850
Wire Wire Line
	5000 3850 5200 3850
Wire Wire Line
	4700 3750 4950 3750
Wire Wire Line
	4950 3750 4950 3950
Wire Wire Line
	4950 3950 5200 3950
Wire Wire Line
	4700 3850 4900 3850
Wire Wire Line
	4900 3850 4900 4050
Wire Wire Line
	4900 4050 5200 4050
Wire Wire Line
	4700 3950 4850 3950
Wire Wire Line
	4850 3950 4850 4150
Wire Wire Line
	4850 4150 5200 4150
Wire Wire Line
	4700 4050 4800 4050
Wire Wire Line
	4800 4050 4800 4250
Wire Wire Line
	4800 4250 5200 4250
Wire Wire Line
	4700 4150 4750 4150
Wire Wire Line
	4750 4150 4750 4350
Wire Wire Line
	4750 4350 5200 4350
Wire Wire Line
	6300 3750 6300 3450
Wire Wire Line
	6300 3450 5700 3450
Wire Wire Line
	5700 3550 6250 3550
Wire Wire Line
	6250 3550 6250 3850
Wire Wire Line
	6250 3850 6300 3850
Wire Wire Line
	5700 3650 6200 3650
Wire Wire Line
	6200 3650 6200 3950
Wire Wire Line
	6200 3950 6300 3950
Wire Wire Line
	5700 3750 6150 3750
Wire Wire Line
	6150 3750 6150 4050
Wire Wire Line
	6150 4050 6300 4050
Wire Wire Line
	5700 3850 6100 3850
Wire Wire Line
	6100 3850 6100 4150
Wire Wire Line
	6100 4150 6300 4150
Wire Wire Line
	6800 4150 6800 4350
Wire Wire Line
	6800 4350 5700 4350
Wire Wire Line
	6800 4050 6850 4050
Wire Wire Line
	6850 4050 6850 4400
Wire Wire Line
	6850 4400 5750 4400
Wire Wire Line
	5750 4400 5750 4250
Wire Wire Line
	5750 4250 5700 4250
Wire Wire Line
	5700 4150 5800 4150
Wire Wire Line
	5800 4150 5800 4450
Wire Wire Line
	5800 4450 6900 4450
Wire Wire Line
	6900 4450 6900 3950
Wire Wire Line
	6900 3950 6800 3950
Wire Wire Line
	6800 3850 6950 3850
Wire Wire Line
	6950 3850 6950 4500
Wire Wire Line
	6950 4500 5850 4500
Wire Wire Line
	5850 4500 5850 4050
Wire Wire Line
	5850 4050 5700 4050
Wire Wire Line
	5700 3950 5900 3950
Wire Wire Line
	5900 3950 5900 4550
Wire Wire Line
	5900 4550 7000 4550
Wire Wire Line
	7000 4550 7000 3750
Wire Wire Line
	7000 3750 6800 3750
$EndSCHEMATC
